import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Livre from "./views/Livre";
import Restaurant from "./views/Restaurant";
import Port from "./views/Port";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/livre',
      name: 'livre',
      component: Livre
    },
    {
      path: '/restaurant',
      name: 'restaurant',
      component: Restaurant
    },
    {
      path: '/port',
      name: 'port',
      component: Port
    }
  ]
})
